#!/bin/bash

mkdir -p out

# More of the MAGD stuff can be found at:
# http://www.ifs.tuwien.ac.at/mir/msd/download.html

wget http://www.ifs.tuwien.ac.at/mir/msd/partitions/msd-MAGD-genreAssignment.cls \
	-O out/genres.cls
