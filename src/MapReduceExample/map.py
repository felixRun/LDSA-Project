#!/usr/bin/env python2

import sys
import hdf5_getters


for line in sys.stdin:
	h5 = hdf5_getters.open_h5_file_read(line.strip())
	numSongs = hdf5_getters.get_num_songs(h5)
	print '%s\t%s' % ('num_songs', numSongs)
	artistName = hdf5_getters.get_artist_name(h5, 0) # Assuming 1 song in the file
	print '%s\t%s' % (artistName, 1)
	h5.close()
