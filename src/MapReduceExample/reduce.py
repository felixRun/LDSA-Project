#!/usr/bin/env python2

import sys

ids_count = {}

for line in sys.stdin:
	line = line.strip()
	identifier, count = line.split('\t', 1)
	try:
		count = int(count)
	except ValueError:
		continue
	if not identifier in ids_count:
		ids_count[identifier] = 0
	ids_count[identifier] += count

for key, value in ids_count.iteritems():
	print '%s\t%s' % (key, value)
