#!/usr/bin/env python2

import tables

def open_h5_file_append(path):
    return tables.open_file(path, mode='a')

def get_genre_hash(file):
    genres = {}

    for line in open(file):
        line = line.strip()
        (track_id, genre) = line.split('\t')
        genres[track_id] = genre

    return genres

def get_track_id(h5):
    return h5.root.analysis.songs.cols.track_id[0]

def update_table_with_genre(h5, genre):
    table = h5.root.metadata.songs
    for row in table.iterrows():
        row['genre'] = genre
        row.update()
    table.flush

