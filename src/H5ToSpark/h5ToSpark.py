
import LDSAProject.lib.MSongsDB.hdf5_getters as hdf5_getters
import os
import sys
from pyspark.mllib.regression import LabeledPoint

def h5_to_object(h5):
	keys = [
		['get_artist_name', 'artist_name'],
		['get_artist_familiarity', 'artist_familiarity'],
		['get_artist_hotttnesss', 'artist_hotttnesss'],
		['get_artist_location', 'artist_location'],
		['get_danceability', 'danceability'],
		['get_duration', 'duration'],
		['get_end_of_fade_in', 'end_of_fade_in'],
		['get_energy', 'energy'],
		['get_key', 'key'],
		['get_loudness', 'loudness'],
		['get_mode', 'mode'],
		['get_tempo', 'tempo'],
		['get_time_signature', 'time_signature'],
		['get_track_id', 'track_id'],
		['get_year', 'year'],
		['get_genre', 'genre'],
	]
	# Might want to add or remove some of the keys.
	o = {}
	for key in keys:
		o[key[1]] = hdf5_getters.__getattribute__(key[0])(h5, 0)
	return o

genres = ['rap', 'pop_rock', 'easy_listening', 'vocal', 'electronic', 'blues',
	'rnb', 'folk', 'religious', 'country', 'jazz', 'new age', 'latin',
	'international', 'comedy_spoken', 'reggae', 'stage', 'children',
	'classical', 'avant_garde', 'holiday']

def map_number_to_genre(key):
	return genres[key]

def map_genre_to_number(genre):
	genre = genre.lower()
	if (genre in genres):
		return genres.index(genre)
	return -1

h5_keys = [ 'get_artist_familiarity', 'get_artist_hotttnesss', 'get_danceability',
'get_duration', 'get_end_of_fade_in', 'get_energy', 'get_key', 'get_loudness',
'get_mode', 'get_tempo', 'get_time_signature', 'get_year', 'get_genre']


def h5_to_labeled_point(h5):
	# Directly into labled points without the object
	flat_map = []
	for key in h5_keys:
		value = hdf5_getters.__getattribute__(key)(h5, 0)
		if not isinstance(value, basestring):
			flat_map.append(value)
	genre = hdf5_getters.get_genre(h5, 0)
	genre_number = map_genre_to_number(genre)
	lp = LabeledPoint(genre_number,  flat_map)
	return lp

def h5_filepath_to_labeled_point(filepath):
	h5 = hdf5_getters.open_h5_file_read(filepath)
	lp = h5_to_labeled_point(h5)
	h5.close()
	return lp

def object_to_labeled_point(o):
	# Creating an object first makes it an extra unnecessary step. Could fix later.
	genre_number = map_genre_to_number(o['genre'])
	flat_map = []
	for key in o:
		if key != 'genre' and not isinstance(o[key], basestring):
			flat_map.append(o[key])
	lp = LabeledPoint(genre_number, flat_map)
	return lp

def h5_filepath_to_object(filepath):
	h5 = hdf5_getters.open_h5_file_read(filepath)
	o = h5_to_object(h5)
	h5.close()
	return o

def read_recursive(path, f, arg1, arg2):
	for root, subdirs, files in os.walk(path):
		for filename in files:
			full_path = os.path.join(root, filename)
			f(full_path, arg1, arg2)

class RddContainer:
	def __init__(self, rdd):
		self.rdd = rdd

def add_object_to_rddcontainer_from_file(filepath, container, sc):
	if (filepath.endswith('.h5')):
		o = h5_filepath_to_object(filepath)
		lp = object_to_labeled_point(o)
		if (lp.label != -1):
			container.rdd = container.rdd.union(sc.parallelize([lp]))

def rootpath_to_rdd(rootpath, sc):
	rdd = sc.parallelize([])
	container = RddContainer(rdd)
	read_recursive(sys.argv[1], add_object_to_rddcontainer_from_file, container, sc)
	return container.rdd

def get_path(path, arr, b):
        arr.append(path)

def build_file_list(path):
        arr = []
        read_recursive(path, get_path, arr, '')
        return arr
