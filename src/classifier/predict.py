#!/usr/bin/env python2

import sys
#import h5py
#from pyspark import SparkContext

model = RandomForestModel.load(sc, "/home/mafr6151/link/magicmike.trees")

#Create RDD out of songs:
#sc = SparkContext(appName="MagicMike_MakesPredictions")
#files=sc.textFile('/home/mafr6151/link/filelist.txt',minPartitions=partitions)
files=open('filelist.txt') #this file has the path to 1 million files

data=[] #the placeholder for the result RDD

#-------------------------------------------------

def parse_file(f):
	placeholder=[] #this is to be replaced with the actual datastructure for a song
	infile=open(f)
	
	for l in infile:	#this is where we read the data and puts it in structure. NO LABELS!
		placeholder.append(l.strip())
	
	return placeholder

#-------------------------------------------------

#data=files.map(parse_file) #we cant do map yet since its not a real RDD so we simulate bahaviour
for line in files:
	data.append(parse_file(line.strip()))
	

print data

predictions = model.predict(testData.map(lambda x: x.features))
output=optional_formatting_function(data).zip(predictions)
