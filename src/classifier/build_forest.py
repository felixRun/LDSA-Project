#!/usr/bin/env python2

#code stolen from
#https://www.hdfgroup.org/2015/03/from-hdf5-datasets-to-apache-spark-rdds/
#https://spark.apache.org/docs/2.1.0/mllib-ensembles.html

import sys
#import h5py
#from pyspark import SparkContext



#Some lines are commented out as this code was written outside of a spark environment....
#Commented out lines are followed by non-spark stuff that mimics the functionality


#-----------------------------------------------------------------------------#
# Function for parsing individual files and returning a labeled feature vector#
#-----------------------------------------------------------------------------#
def parse_file(f):
	placeholder=[] #this is to be replaced with the actual datastructure for a song
	infile=open(f)
	
	#replace with actual code for reading the label
	label="dummy"

	#replace with actual code for reading the selected features
	for l in infile:	
		placeholder.append(l.strip())

	infile.close()
	
	return [label, placeholder] 	#the placeholder should be replaced with the structure that the prediction model likes.
					#presumably it is a list: [label [feature_1,...,feature_n]]

#-----------------------------------------------------------------------------#
#                            Main function kinda...                           #
#-----------------------------------------------------------------------------#

#we cant do map yet since its not a real RDD so we simulate bahaviour
#Create RDD out of songs:
#sc = SparkContext(appName="MagicMike_BuildsTrees")
#
#partitions=7    #i just guessed this number. dont know what it is. maybe it can be omitted.
#files=sc.textFile('/home/mafr6151/link/filelist.txt',minPartitions=partitions)
files=open('filelist.txt') #this file has the path to labled songs
data=[] #the placeholder for the result RDD



for line in files:
	data.append(parse_file(line.strip()))
#data=files.map(parse_file) #<--- replacement code
	

print data #this is probably not to reccomend for the full dataset :)

sys.exit() #no chance the following code runs outside a spark context, so we bail out


#----------------------------------------------------------------------------#
# Here comes the part that we just hope will run. No testing was possible :) #
#----------------------------------------------------------------------------#

#it's expected from here on that data has a particular format that we might need to look into.
#Schematically, entries look like this: data[72]=[double label,[double feature_1, ..., feature_n]]

# Split the data into training and test sets (30% held out for testing)
(trainingData, testData) = data.randomSplit([0.7, 0.3])
model = RandomForest.trainClassifier(trainingData, numClasses=2, categoricalFeaturesInfo={},
                                     numTrees=3, featureSubsetStrategy="auto",
                                     impurity='gini', maxDepth=4, maxBins=32)

# Evaluate model on test instances and compute test error

predictions = model.predict(testData.map(lambda x: x.features))
labelsAndPredictions = testData.map(lambda lp: lp.label).zip(predictions)
testErr = labelsAndPredictions.filter(lambda (v, p): v != p).count() / float(testData.count())
print('Test Error = ' + str(testErr))
print('Learned classification forest model:')
print(model.toDebugString())

# Save and load model
model.save(sc, "/home/mafr6151/link/magicmike.trees")
