Classifier for the million songs dataset
========================================

Genres
------

To run the extender example for the genres we need to download the genres. Currently this is done via the script

```
./get_genres.sh
```

This fetches the file with location http://www.ifs.tuwien.ac.at/mir/msd/partitions/msd-MAGD-genreAssignment.cls in `out/genres.cls`. More info about MAGD is here: 
http://www.ifs.tuwien.ac.at/mir/msd/download.html

Next step is to create a text file with all the paths. That is done using the command

```
./examples/build_file_list.py path/to/h5/folder h5_file_paths.txt
```

Next step is to run the genre extender example:

```
./examples/h5_genre_extender.py out/genres.cls h5_file_paths.txt
```

The files will be extended with the genre.


Running random forest on the data set
-------------------------------------

To test the Spark part you need to download Spark and put it in your `PATH` environment variable. Download Spark for example from:

```
wget https://d3kbcqa49mib13.cloudfront.net/spark-2.1.1-bin-hadoop2.7.tgz
```

Then unpack and put the `./bin` in `PATH`. More on the Spark downloads: https://spark.apache.org/downloads.html

Assuming the Genre section is done above, and a master is up and running with working slaves, the following
is an example command to run the simulations of random forest classification.

```
spark-submit --total-executor-cores 32 --master "spark://192.168.1.36:7077" h5_to_spark.py h5_file_paths.txt > results/result32.txt 2>&1
```


Environment setup
-----------------

```
sudo apt-get install python
sudo apt-get install openjdk-7-jre
sudo apt-get install python-pip
pip2 install tables

ssh-keygen -t rsa -P "" in ~/.ssh
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys

export PYTHONPATH=$PYTHONPATH:.
```




