import pyspark
import sys
import os

### ZIPPING THE PACKAGE
#import dictconfig
import zipfile

def ziplib():
    libpath = os.path.dirname(__file__)
    middlename = '123123'
    zippath = '/tmp/mylib-' + middlename + '.zip'
    zf = zipfile.PyZipFile(zippath, mode='w')
    try:
        zf.debug = 3
        zf.writepy(libpath)
        return zippath
    finally:
        zf.close()

zip_path = ziplib()
sc = pyspark.SparkContext('local', 'app')
sc.addPyFile(zip_path)
#rdd = h5ToSpark.rootpath_to_rdd(sys.argv[1], sc)

#import lib.MSongsDB.hdf5_getters as hdf5_getters
import hdf5_getters
import src.H5ToSpark.h5ToSpark as h5ToSpark

#print rdd.collect()

rdd_file = sc.textFile(sys.argv[1])

def h5_to_labeled_point(filepath):
    return h5ToSpark.object_to_labeled_point(h5ToSpark.h5_filepath_to_object(filepath))

dataset = rdd_file.map(h5_to_labeled_point)

print dataset.collect()

os.remove(zip_path)
