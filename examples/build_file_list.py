#!/usr/bin/env python2

import sys
import os

def read_recursive(path, f, arg1, arg2):
	for root, subdirs, files in os.walk(path):
		for filename in files:
			full_path = os.path.join(root, filename)
			f(full_path, arg1, arg2)

def get_path(path, arr, b):
    if (path.endswith('.h5')):
        arr.append(path)

def build_file_list(path):
    arr = []
    read_recursive(path, get_path, arr, '')
    return arr


arr = build_file_list(sys.argv[1])

file = open(sys.argv[2], 'w')

for line in arr:
    file.write('%s\n' % line)

file.close()
