#!/usr/bin/env python2

import src.H5GenreExtender.h5genreExtender as h5genreExtender
import sys
import tables
import time

genres = h5genreExtender.get_genre_hash(sys.argv[1])

count = 0

time_start = time.time()

with open(sys.argv[2]) as f:
    for line in f:
        h5 = h5genreExtender.open_h5_file_append(line.strip())
        track_id = h5genreExtender.get_track_id(h5)
        if track_id in genres:
            h5genreExtender.update_table_with_genre(h5, genres[track_id])
        h5.close()
        count += 1
        if count % 1000 == 0:
            time_current = time.time() - time_start
            print "We have processed %s files! Elapsed %s." % (count, time_current)

