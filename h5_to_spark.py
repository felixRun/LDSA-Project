import pyspark
import sys
import os
import time
from pyspark.mllib.tree import RandomForest

### ZIPPING THE PACKAGE
#import dictconfig
import zipfile

def ziplib():
    libpath = os.path.dirname(__file__)
    middlename = '123123'
    zippath = '/tmp/mylib-' + middlename + '.zip'
    zf = zipfile.PyZipFile(zippath, mode='w')
    try:
        zf.debug = 3
        zf.writepy(libpath)
        return zippath
    finally:
        zf.close()

zip_path = ziplib()

#master = "spark://group1-project:7077"
master = "spark://192.168.1.36:7077"
#master = "local[*]"
sparkConf = pyspark.SparkConf().setAppName('appname').setMaster(master)
sc = pyspark.SparkContext(conf=sparkConf)
print "================== Def. Parallelism: %s" % sc.defaultParallelism

#sc = pyspark.SparkContext('local', 'app')
sc.addPyFile(zip_path)
#rdd = h5ToSpark.rootpath_to_rdd(sys.argv[1], sc)

#import lib.MSongsDB.hdf5_getters as hdf5_getters
#import hdf5_getters
print os.getcwd()

import LDSAProject.src.H5ToSpark.h5ToSpark as h5ToSpark

#import h5ToSpark

#print rdd.collect()

rdd_file = sc.textFile(sys.argv[1], 100)

def h5_to_labeled_point(filepath):
    return h5ToSpark.h5_filepath_to_labeled_point(filepath)
    #return h5ToSpark.object_to_labeled_point(h5ToSpark.h5_filepath_to_object(filepath))

def contains_genre(lp):
    return lp.label != -1

start_time = time.time()

print '==================== Read files'
dataset = rdd_file.map(h5_to_labeled_point)
print '==================== Files are read'
dataset = dataset.filter(contains_genre)
print '==================== Filtered labeled data'
time_filtering = time.time()
#print '==================== %s' % len(dataset.collect())
#time_collect = time.time()

(trainingData, testData) = dataset.randomSplit([0.7, 0.3])
time_split = time.time()
print '==================== Finished splitting, now randomforest'

#model = RandomForest.trainClassifier(trainingData, numClasses=2, categoricalFeaturesInfo={},
#                                     numTrees=3, featureSubsetStrategy="auto",
#                                     impurity='gini', maxDepth=4, maxBins=32)
model = RandomForest.trainClassifier(trainingData, numTrees=16, numClasses=21,
                                     categoricalFeaturesInfo={})
                                     
time_training = time.time()
print '==================== Finished training'

predictions = model.predict(testData.map(lambda x: x.features))
time_prediction = time.time()

print '==================== Finished getting predictions'

labelsAndPredictions = testData.map(lambda lp: lp.label).zip(predictions)
print '==================== labelsAndPredictions gotten'
testErr = labelsAndPredictions.filter(lambda (v, p): v != p).count() / float(testData.count())
print '==================== Test error calculated'
print('Test Error = ' + str(testErr))
print('Learned classification forest model:')
print(model.toDebugString())

time_finished = time.time()-start_time

print "RESULT time start: %s" % start_time
print "RESULT time filtering: %s" % (time_filtering - start_time)
#print "RESULT time collect: %s" % (time_collect - time_filtering)
print "RESULT time split: %s" % (time_split - time_filtering)
print "RESULT time training: %s" % (time_training - time_split)
print "RESULT time prediction: %s" % (time_prediction - time_training)
print "RESULT overall time: %s" % time_finished

#print dataset.collect()
model_path = "./models/rf_model_%s.trees" % time.time()
model.save(sc, model_path)
os.remove(zip_path)
