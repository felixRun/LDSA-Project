#!/bin/bash

~/spark/bin/spark-submit --total-executor-cores 32 --master "spark://192.168.1.36:7077" h5_to_spark.py BC.txt > results/BC32_16trees.txt 2>&1

~/spark/bin/spark-submit --total-executor-cores 32 --master "spark://192.168.1.36:7077" h5_to_spark.py out_bigger.txt > results/ABC32_16trees.txt 2>&1

~/spark/bin/spark-submit --total-executor-cores 16 --master "spark://192.168.1.36:7077" h5_to_spark.py out_big.txt > results/A16_16trees.txt 2>&1
~/spark/bin/spark-submit --total-executor-cores 8 --master "spark://192.168.1.36:7077" h5_to_spark.py out_big.txt > results/A8_16trees.txt 2>&1
~/spark/bin/spark-submit --total-executor-cores 4 --master "spark://192.168.1.36:7077" h5_to_spark.py out_big.txt > results/A4_16trees.txt 2>&1
~/spark/bin/spark-submit --total-executor-cores 2 --master "spark://192.168.1.36:7077" h5_to_spark.py out_big.txt > results/A2_16trees.txt 2>&1
~/spark/bin/spark-submit --total-executor-cores 1 --master "spark://192.168.1.36:7077" h5_to_spark.py out_big.txt > results/A1_16trees.txt 2>&1


