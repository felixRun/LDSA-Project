# -*- coding: utf-8 -*-
"""
Created on Thu May 25 09:57:28 2017

@author: Tea
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
#########################Results for 4000 songs
results = pd.read_csv('/Users/Tea/Documents/Bioinformatics Program/LDSA/results4000.csv')#, encoding='utf-8')
results['minutes']=results['time']/60
results['train_minutes']=results['training']/60
results
sorted_res=results.sort_values('Cores')
params = {'legend.fontsize': 'large',
          'figure.figsize':(10,5),
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'axes.labelweight':'bold',
         'xtick.labelsize':'large',
         'ytick.labelsize':'large'}
pylab.rcParams.update(params)

x4= np.array(sorted_res['Cores'])
# different graph for the fast processes
fig, (ax, ax1) = plt.subplots(nrows=1, ncols=2)
line3, = ax.plot(x4, np.array(sorted_res['training']),linewidth=2, label='Training')
line5, = ax.plot(x4, np.array(sorted_res['time']),linewidth=2, label='Total time')
ax.set_ylabel('Time in seconds')
ax.set_xlabel('Number of cores')
ax.legend(loc='upper right')

line4, = ax1.plot(x4, np.array(sorted_res['prediction']),linewidth=2, label="Testing")
ax1.set_xlabel('Number of cores')
ax1.legend(loc='upper right')
ax2 = ax1.twinx()
line1, = ax2.plot(x4,np.array(sorted_res['filtering']),  'r--', linewidth=2, label='Filtering')
line2, = ax2.plot(x4, np.array(sorted_res['split']), 'r-..',linewidth=2,label='Split')
ax2.set_ylabel('Time in seconds',color='red' )
ax2.legend(loc='upper right', bbox_to_anchor=(1,0.95))
fig.tight_layout()
plt.show()
##################Tree testing
#trying traditionally
treetest2 = pd.read_csv('/Users/Tea/Documents/Bioinformatics Program/LDSA/tree_testing.csv')#, encoding='utf-8')
set=[]
newset=pd.DataFrame()
newset['Cores']=treetest2['Cores']
newset['trees']=treetest2['trees']
newset['training']=treetest2['training']
newset['total time']=treetest2['time']

sns.set_style('whitegrid')
xt=newset['Cores']
g= sns.factorplot(x=xt, y= 'training', hue='trees',data=newset, kind='bar' )
g.set_ylabels("Training")

sns.set_style('whitegrid')
xt=newset['Cores']
g= sns.factorplot(x=xt, y= 'total time', hue='trees',data=newset, kind='bar' )
g.set_ylabels("Total time")
####Figures on all 39000 trees comparisons

Atest = pd.read_csv('/Users/Tea/Documents/Bioinformatics Program/LDSA/project_results.csv')#, encoding='utf-8') #results from all runs with 39000 songs
A4list=[]
a5=pd.DataFrame(A4list)
a5=Atest[Atest.workers==4][['training','Cores', 'time', 'filtering', 'split', 'prediction']]
sns.set_style('whitegrid')
ax = sns.pointplot(x='Cores', y='time', data=a5)
ax.set_ylabel('Total time')
##and one figure like for 4000 songs with the small times too
sns.set_style('white')
x=np.array(a5['Cores'])
fig, (ax, ax1) = plt.subplots(nrows=1, ncols=2)
line3, = ax.plot(x, np.array(a5['training']),linewidth=2, label='Training')
line5, = ax.plot(x, np.array(a5['time']),linewidth=2, label='Total time')
ax.set_ylabel('Time in seconds')
ax.set_xlabel('Number of cores')
ax.legend(loc='upper right')

line4, = ax1.plot(x, np.array(a5['prediction']),linewidth=2, label="Testing")
ax1.set_xlabel('Number of cores')
ax1.legend(loc='upper right',bbox_to_anchor=(1,0.95))
ax2 = ax1.twinx()
line1, = ax2.plot(x,np.array(a5['filtering']),  'r--', linewidth=2, label='Filtering')
line2, = ax2.plot(x, np.array(a5['split']), 'r-..',linewidth=2,label='Split')
ax2.set_ylabel('Time in seconds',color='red' )
ax2.legend(loc='upper right', bbox_to_anchor=(1,0.89))
fig.tight_layout()
plt.show()

#####Figures for comparing data amounts
datatest = pd.read_csv('/Users/Tea/Documents/Bioinformatics Program/LDSA/data_amount.csv')#, encoding='utf-8')
sorted_data=datatest.sort_values('Cores')
sns.set_style('whitegrid')
plt.rcParams['figure.figsize']=(7,5)
ax = sns.pointplot(x='Songs', y='time', data=sorted_data)
ax.set_ylabel('Total time')

###Figures comparing the number of workers (1,2,4) for datasetA = 39000 songs
work=Atest[Atest.Cores!=32][['training','workers', 'time', 'Cores'] ]
sns.set_style('whitegrid')
ax = sns.factorplot(x='Cores', y='time', hue = 'workers' ,data=work)
ax.set_ylabel('Total time')
######OR
sns.set_style('white')
plt.rcParams['figure.figsize']=(7,5)
ax = sns.barplot(x='Cores', y='time', hue = 'workers' ,data=work)
ax.set_ylabel('Total time')
######compare 1 worker 8 cores, 2 workers 16 cores and 4 workers 32 cores
horiz=Atest[(Atest.Cores==8)|(Atest.Cores==16)|(Atest.Cores==32)][['time', 'Cores', 'workers']]
#subset without 2 & 4 cores with 8 workers
horiz2=horiz.drop(horiz[(horiz.Cores==8)&((horiz.workers==2)|(horiz.workers==4))].index)
#subset without 4 cores with 16 workers
horiz3=horiz2.drop(horiz2[(horiz2.Cores==16)&(horiz2.workers==4)].index)
plt.rcParams['figure.figsize']=(7,5)
fig, ax1=plt.subplots()
sns.set_style('whitegrid')
ax1=plt.plot(horiz3['workers'], horiz3['time'] )
ax1=plt.scatter(horiz3['workers'], horiz3['time'],color=['blue', 'green', 'orange'] , s=86)
pylab.ylabel('Total time')
pylab.xlabel('Workers')
plt.show()
