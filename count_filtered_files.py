import pyspark
import sys
import os
import time
from pyspark.mllib.tree import RandomForest

### ZIPPING THE PACKAGE
#import dictconfig
import zipfile

def ziplib():
    libpath = os.path.dirname(__file__)
    middlename = '123123'
    zippath = '/tmp/mylib-' + middlename + '.zip'
    zf = zipfile.PyZipFile(zippath, mode='w')
    try:
        zf.debug = 3
        zf.writepy(libpath)
        return zippath
    finally:
        zf.close()

zip_path = ziplib()

#master = "spark://group1-project:7077"
master = "spark://192.168.1.36:7077"
#master = "local[*]"
sparkConf = pyspark.SparkConf().setAppName('appname').setMaster(master)
sc = pyspark.SparkContext(conf=sparkConf)
print "================== Def. Parallelism: %s" % sc.defaultParallelism

#sc = pyspark.SparkContext('local', 'app')
sc.addPyFile(zip_path)
#rdd = h5ToSpark.rootpath_to_rdd(sys.argv[1], sc)

#import lib.MSongsDB.hdf5_getters as hdf5_getters
#import hdf5_getters
print os.getcwd()

import LDSAProject.src.H5ToSpark.h5ToSpark as h5ToSpark

#import h5ToSpark

#print rdd.collect()

rdd_file = sc.textFile(sys.argv[1], 100)

def h5_to_labeled_point(filepath):
    return h5ToSpark.h5_filepath_to_labeled_point(filepath)
    #return h5ToSpark.object_to_labeled_point(h5ToSpark.h5_filepath_to_object(filepath))

def contains_genre(lp):
    return lp.label != -1

start_time = time.time()

print '==================== Read files'
dataset = rdd_file.map(h5_to_labeled_point)
print '==================== Non-filtered dataset, number of files: %s' % len(dataset.collect())
dataset = dataset.filter(contains_genre)
print '==================== Filtered dataset, number of files: %s' % len(dataset.collect())
